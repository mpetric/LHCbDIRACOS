variables:
    # Used by the deploy job
    EOS_PATH: "/eos/project/l/lhcbwebsites/www/lhcb-rpm/LHCbDIRACOS/"
    DIRACOS_GIT: "https://github.com/DIRACGrid/DIRACOS.git"


stages:
    - changelog
    - compile
    - links
    - test
    - deploy
    - push


compile:
  stage: compile
  tags:
    - docker-privileged
  retry: 2
  except:
    - tags
  only:
    - branches
  image: diracgrid/centos6
  before_script:
    - yum install -y https://diracos.web.cern.ch/diracos/bootstrap/jq-1.5-1.el6.art.x86_64.rpm git python-pip
    - git clone ${DIRACOS_GIT}
  script:
    - mkdir changes
    - source tools/CreateName.sh
    # Set the version of the configuration file to the BUILD_NAME
    # and the DIRACOS version to DOS_NAME
    - jq ".version = \"${BUILD_NAME}\" | .diracOsVersion = \"${DOS_NAME}\"" config/lhcbdiracos.json > config/lhcbdiracos.json.tmp && mv config/lhcbdiracos.json.tmp config/lhcbdiracos.json
    - cp config/lhcbdiracos.json changes/lhcbdiracos.json
    # Install DIRACOS python packages
    - pip install git+${DIRACOS_GIT}
    # Fix the python packages versions
    - fixedVersion=$(dos-fix-pip-versions config/lhcbdiracos.json | tail -n 1|  awk {'print $NF'})
    - cp $fixedVersion changes/lhcb_requirements.txt
    # Replace the requirements.txt with the fixed version
    - cp $fixedVersion config/lhcb_requirements.txt
    - mkdir -p public/releases
    # Building the extension
    - tarLocation=$(dos-build-extension config/lhcbdiracos.json | tail -n 1 | awk {'print $NF'})
    - cp ${tarLocation} public/releases/
    - cd public/releases
    - md5sum LHCbdiracos-${BUILD_NAME}.tar.gz | awk {'print $1'} > LHCbdiracos-${BUILD_NAME}.md5

  artifacts:
    paths:
      - public
      - changes
    expire_in: 1 week


# Run tests

.run_test:
  stage: test
  dependencies:
    - compile
  tags:
    - docker
  only:
    - branches
  except:
    - tags
  script:
    - git clone ${DIRACOS_GIT}
    - source tools/CreateName.sh
    - tar xf public/releases/LHCbdiracos-${BUILD_NAME}.tar.gz -C /tmp
    - export DIRACOS=/tmp/diracos
    - source $DIRACOS/diracosrc
    - pytest tests/integration/test_import.py

SLC6:
  extends: .run_test
  image: diracgrid/centos6
  before_script:
    - yum install -y git
    # graphical libraries needed for rrdtool
    - yum install -y tar freetype fontconfig pixman libXrender

CC7:
  extends: .run_test
  image: cern/cc7-base
  before_script:
    - yum install -y git
    # graphical libraries needed for rrdtool
    - yum install -y freetype fontconfig pixman libXrender

fedora-latest:
  extends: .run_test
  image: fedora:latest
  allow_failure: true
  before_script:
    - yum install -y git
    # Needed for ARC
    - yum install -y libnsl libxcrypt-compat

ubuntu-latest:
  extends: .run_test
  image: ubuntu:latest
  allow_failure: true
  before_script:
    - apt update
    - apt install -y git


# Deploy to EOS folder
deployment:
    stage: deploy
    dependencies:
      - compile
    except:
      - tags
    only:
      - branches@lhcb-dirac/LHCbDIRACOS
    # Custom docker image providing the needed tools to deploy in EOS
    image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
    script:
        - deploy-eos
    # do not run any globally defined before_script or after_script for this step
    before_script: []
    after_script: []

# Update release notes and tag
push_tag:
  stage: push
  dependencies:
    # - changelog
    - compile
  only:
    # - branches@CLICdp/iLCDirac/DIRACOS
    refs:
      - branches@lhcb-dirac/LHCbDIRACOS
    variables:
      - $CI_COMMIT_REF_NAME =~ /rel-.*/
  tags:
    - docker
  artifacts:
    paths:
      - public
      - changes
    expire_in: 1 week
  variables:
    GIT_STRATEGY: none
  image: cern/cc7-base
  before_script:
    - curl -O -L https://diracos.web.cern.ch/diracos/bootstrap/get-pip.py
    - python get-pip.py pip==20.2.4
    - pip install -U requests python-dateutil pytz
    - yum install git less -y
    - git config --global user.email "lhcbdirac.os@cern.ch"
    - git config --global user.name "LHCbDIRACOS"
    - git config --global pull.rebase true
  script:
    - git clone https://${EOS_ACCOUNT_USERNAME}:${DEPLOY_KEY}@gitlab.cern.ch/${CI_PROJECT_PATH}.git
    - cd LHCbDIRACOS
    - git clone ${DIRACOS_GIT}
    # Populate BUILD_NAME,DOS_NAME amd MAKE_TAG
    - source tools/CreateName.sh
    - mkdir bundle
    - tar xf ../public/releases/LHCbdiracos-${BUILD_NAME}.tar.gz -C bundle

    ### DO THE RELEASE NOTES HERE
    - curl -O -L https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/docs/diracdoctools/scripts/dirac-docs-get-release-notes.py
    - chmod +x dirac-docs-get-release-notes.py
    - echo "GITLABTOKEN = \"${DEPLOY_KEY}\"" >& GitTokens.py
    - export PYTHONPATH=$(pwd)
    # Since we always merge in master, we should use --branches master
    # But then we replace the [master] tag with the build name
    # We also add the content of the LHCb_versions.txt file, except the header
    # which repeates the tag name
    - cat bundle/diracos/LHCb_versions.txt | sed '/^LHCbDIRACOS/d' > /tmp/LHCb_versions.txt
    - export CHANGE_FILE=../changes/versionChanges.txt
    - python dirac-docs-get-release-notes.py -g -i ${CI_PROJECT_ID} --branches master --sinceLatestTag --headerMessage "Based on DIRACOS $DOS_NAME" --footerMessage /tmp/LHCb_versions.txt  > "${CHANGE_FILE}"
    # It may be that there were no changes on LHCbDIRACOS side, but only on DIRACOS, in which case
    # the changelog would be generated without the [master] tag. If that's the case, we just add the correct tag
    - sed -E -e "1s/\[master\]/[${BUILD_NAME}]/" -e "1s/Based on/[${BUILD_NAME}]\n\n\0/" -i "${CHANGE_FILE}"
    - cat "${CHANGE_FILE}" release.notes > ../changes/release.notes

    ### END RELEASE NOTES ###

    # We push back to the master branch only the change log

    - cp ../changes/release.notes release.notes
    - git add release.notes
    - git commit -m "Add release notes for ${BUILD_NAME}"

    - if $MAKE_TAG ; then git push origin master ; fi

    # Now we can also commit the changes in lhcb_requirements.txt and lhcbdiracos.json and create the tag

    - cp ../changes/lhcb_requirements.txt config/lhcb_requirements.txt
    - git add config/lhcb_requirements.txt
    - cp ../changes/lhcbdiracos.json config/lhcbdiracos.json
    - git add config/lhcbdiracos.json
    - git commit -m "Update requirements.txt and diracos.json for release ${BUILD_NAME}"
    - git show
    - git tag -a ${BUILD_NAME} -m "${BUILD_NAME}"
    - if $MAKE_TAG ; then git push --tags origin ; fi

    # Create the gitlab release
    - if $MAKE_TAG ; then python dirac-docs-get-release-notes.py -ddd -g -i ${CI_PROJECT_ID} --deployRelease --releaseNotes "${CHANGE_FILE}"  --tagName ${BUILD_NAME}  ; fi




# test_tag:
#   stage: push
#   only:
#     # - branches@CLICdp/iLCDirac/DIRACOS
#     - branches
#   tags:
#     - docker
#   variables:
#     GIT_STRATEGY: none
#   image: cern/cc7-base
#   before_script:
#     - curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
#     - python get-pip.py
#     - pip install requests
#     - yum install git less -y
#     - git config --global user.email "lhcbdirac.os@cern.ch"
#     - git config --global user.name "LHCbDIRACOS"
#     - git config --global pull.rebase true
#   script:
#     - echo "https://${EOS_ACCOUNT_USERNAME}:${DEPLOY_KEY}@gitlab.cern.ch/${CI_PROJECT_PATH}"
#     - git clone https://${EOS_ACCOUNT_USERNAME}:${DEPLOY_KEY}@gitlab.cern.ch/${CI_PROJECT_PATH}.git
#     # - git clone ${CI_REPOSITORY_URL}
#     - cd LHCbDIRACOS
#     - echo toto >> toto
#     - git add toto
#     - git commit -m toto
#     - git push origin test:toto
#     - git show
