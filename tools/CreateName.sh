#!/bin/bash

# if doing a release, we expect the name to be
# rel-XYZ_UVW
# where XYZ is the LHCbDIRACOS release
# and UVW is the DIRACOS version to start from
# If just a test build, we expect
# XYZ_UVW

if [[ $CI_COMMIT_REF_NAME == rel-* ]]; then
  export BUILD_NAME=$(echo ${CI_COMMIT_REF_NAME:4} | awk -F '_' {'print $1'})
  export DOS_NAME=$(echo ${CI_COMMIT_REF_NAME:4} | awk -F '_' {'print $2'})
  if [ -z ${DOS_NAME} ]; then export DOS_NAME="master"; fi
  export MAKE_TAG=true
else
  export BUILD_NAME=$(echo ${CI_COMMIT_REF_NAME} | awk -F '_' {'print $1'})
  export DOS_NAME=$(echo ${CI_COMMIT_REF_NAME} | awk -F '_' {'print $2'})
  if [ -z ${DOS_NAME} ]; then export DOS_NAME="master"; fi
  export MAKE_TAG=false
fi
